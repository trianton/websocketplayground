package id.mapan.websocketplayground.basic

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import id.mapan.proto_model.generateMessageIndo
import id.mapan.websocketplayground.R
import org.java_websocket.client.WebSocketClient
import org.java_websocket.handshake.ServerHandshake
import java.net.URI
import java.net.URISyntaxException
import java.nio.ByteBuffer

class WebsocketBasicActivity : AppCompatActivity() {
    private var mWebSocketClient: WebSocketClient? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        connectWebSocket()
    }

    private fun connectWebSocket() {
        val uri: URI
        try {
            uri = URI("wss://chat-poc-dev.mapan.id/conn")
        } catch (e: URISyntaxException) {
            Log.e("Websocket", "" + e.message)
            return
        }
        mWebSocketClient = object : WebSocketClient(uri) {
            override fun onOpen(serverHandshake: ServerHandshake) {
                Log.i("Websocket", "Opened")
            }

            override fun onMessage(s: String) {
                Log.i("Websocket", s)
            }

            override fun onMessage(bytes: ByteBuffer?) {
                super.onMessage(bytes)
                Log.i("Websocket", "onMessageBytes $bytes")
            }

            override fun onClose(i: Int, s: String, b: Boolean) {
                Log.i("Websocket", "Closed $s")
            }

            override fun onError(e: Exception) {
                Log.i("Websocket", "Error " + e.message)
            }
        }
        mWebSocketClient?.addHeader("X-User-ID", "ronaldo")
        mWebSocketClient?.addHeader(
            "Authorization",
            "Bearer eyJhbGciOiJkaXIiLCJjdHkiOiJKV1QiLCJlbmMiOiJBMjU2R0NNIiwidHlwIjoiSldUIiwiemlwIjoiREVGIn0..ns0kcYFjPpBF9cZd.et_F2zVZc8Kf7QH9a5w6xypFOTfCrX10DbzwWJu57gucE2yoi4k-DqTsTQtmlw_HTAs60nMWwFPjdL7NLxNCPPqAg7rGNzVm79wfOgp05-xFvH5Iv6jt5Yo0949msDF0j0RQ4iHn6G9wa_TfFiWXcmMwUfviN1pWI3K2K6qmKaQY-fefo4mJ0bZ_UNQGzHUU_RcM5bQIZpZ-RmB0-iM3ToE-KF1SWEEDGBD948KgfDjHfbxPVyixl5hSXYK9QSA82kW8e887Hybdix1enDPiLVhT-3gj4N0e7SalgjpT4AHfKoL1KJhH5Zfr9_9kRdAWy0VmE_cXLDkldjlJR1C8djyeyKcwzgk-V6Nb_-jLsN2zZzf2BUDn1IPmySeDYsIcYMOuhV6tqZHJpXKMQpOp9uteszYh25Ue0G-LSauv3_h5v8Hg5hKY0M6H3_J-71AQ5JG4Ol9dL_qqo9Tgq1hvMyBVWRqO8SVasRJL2MCMNY-1SgW7uZ5uMetLZ7x6BKazH868aqASWFa7vz1ah6iO2sE4abC1mqP7rwDtczVG-yVsdkA398g7s69DGwrUQIXPKjrOeEmLgaXZOVG3-Q3wmR6u1phoB0XtI-HK1dSIg7BLFaTYJMR7tjAt9H6yIInN0d02tD3egP_oReuOXohKT5UZnN223OnuHo27Y9qIITv4IVU9yYeZJjNLqnRXgLZY7MZzDGJFBVNYu9k9FtiYbe0UpjcemM89KIXiZVO712cNSnfxDwW4lsn_pQx42OuA7e07P01cePyG-RlOg6QN57foBuVkA2qzKk6-PfZmcrDn64F_5d37rYGMtfzWzzB3mBSkN7zaCFUxG5LrXz9ZZK8wBI5pF27Fjm1WuJTgnxOrrkL1rmFiqv1_gPTw7JGydsEa-Gm5ZP5rEDJ521608bm_aRyjWJbQJUb-vgapK29lUJ-gKj4rd5xdSePGIn3k.ldtFkUXS7v316YzUGUjx1w"
        )
        mWebSocketClient?.connect()
    }

    private fun sendMessage() {
        mWebSocketClient?.let {
            if (it.isOpen) {
                val infomsg = generateMessageIndo()
                it.send(infomsg)
            }
        }
    }
}