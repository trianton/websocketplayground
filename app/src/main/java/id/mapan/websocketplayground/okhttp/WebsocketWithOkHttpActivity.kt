package id.mapan.websocketplayground.okhttp

import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import id.mapan.proto_model.generateMessageIndo
import id.mapan.websocketplayground.R
import okio.ByteString
import okio.ByteString.Companion.toByteString
import kotlin.concurrent.thread

/**
 * Created by Naupal T. on 29/10/21.
 */
class WebsocketWithOkHttpActivity : AppCompatActivity(), MessageListener {
    lateinit var textMessage: TextView
    lateinit var btnOpen: Button
    lateinit var btnSend: Button
    lateinit var etMessage: EditText
    private val serverUrl = "wss://chat-poc-dev.mapan.id/conn"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        textMessage = findViewById(R.id.txtMessage)
        btnOpen = findViewById(R.id.btnOpen)
        btnSend = findViewById(R.id.btnSend)
        etMessage = findViewById(R.id.etMessage)

        WebSocketManager.init(serverUrl, this)

        btnOpen.setOnClickListener {
            thread {
                kotlin.run {
                    WebSocketManager.connect()
                }
            }
        }

        btnSend.setOnClickListener {
            val msg = etMessage.text.toString()
            if (msg.isNotEmpty()) {
                val proto  = generateMessageIndo()
                proto?.let{
                    val bs : ByteString = it.toByteString()
                    if (WebSocketManager.sendMessageByte(bs)) {
                        addText("send - Payload")
                    }
                }
            }
        }
    }


    override fun onConnectSuccess() {
        addText(" Connected successfully \n ")
    }

    override fun onConnectFailed() {
        addText(" Connection failed \n ")
    }

    override fun onClose() {
        addText(" Closed successfully \n ")
    }

    override fun onMessage(text: String?) {
        addText(" Receive message: $text \n ")
        Log.i("Receive message", " $text")
    }

    private fun addText(text: String?) {
        text?.let {
            runOnUiThread {
                textMessage.text = "${textMessage.text.toString()} \n$text"
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        WebSocketManager.close()
    }
}