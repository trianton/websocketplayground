package id.mapan.websocketplayground.okhttp

/**
 * Created by Naupal T. on 30/10/21.
 */

interface MessageListener {
    fun  onConnectSuccess () // successfully connected
    fun  onConnectFailed () // connection failed
    fun  onClose () // close
    fun onMessage(text: String?)
}