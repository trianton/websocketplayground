package id.mapan.websocketplayground.okhttp

import  android.util.Log
import id.mapan.proto_model.generateMessageIndo
import okhttp3.*
import okio.ByteString
import  java.util.concurrent.TimeUnit

/**
 * Created by Naupal T. on 30/10/21.
 */


object WebSocketManager {
    private val TAG = WebSocketManager::class.java.simpleName
    private const val MAX_NUM = 5  // Maximum number of reconnections
    private const val MILLIS = 5000  // Reconnection interval, milliseconds
    private lateinit var client: OkHttpClient
    private lateinit var request: Request
    private lateinit var messageListener: MessageListener
    private lateinit var mWebSocket: WebSocket
    private var isConnect = false
    private var connectNum = 0

    fun init(url: String, _messageListener: MessageListener) {
        client = OkHttpClient.Builder()
            .writeTimeout(5, TimeUnit.SECONDS)
            .readTimeout(5, TimeUnit.SECONDS)
            .connectTimeout(10, TimeUnit.SECONDS)
            .build()
        request = Request.Builder()
            .header("X-User-ID", "ronaldo")
            .header(
                "Authorization",
                "Bearer eyJhbGciOiJkaXIiLCJjdHkiOiJKV1QiLCJlbmMiOiJBMjU2R0NNIiwidHlwIjoiSldUIiwiemlwIjoiREVGIn0..ns0kcYFjPpBF9cZd.et_F2zVZc8Kf7QH9a5w6xypFOTfCrX10DbzwWJu57gucE2yoi4k-DqTsTQtmlw_HTAs60nMWwFPjdL7NLxNCPPqAg7rGNzVm79wfOgp05-xFvH5Iv6jt5Yo0949msDF0j0RQ4iHn6G9wa_TfFiWXcmMwUfviN1pWI3K2K6qmKaQY-fefo4mJ0bZ_UNQGzHUU_RcM5bQIZpZ-RmB0-iM3ToE-KF1SWEEDGBD948KgfDjHfbxPVyixl5hSXYK9QSA82kW8e887Hybdix1enDPiLVhT-3gj4N0e7SalgjpT4AHfKoL1KJhH5Zfr9_9kRdAWy0VmE_cXLDkldjlJR1C8djyeyKcwzgk-V6Nb_-jLsN2zZzf2BUDn1IPmySeDYsIcYMOuhV6tqZHJpXKMQpOp9uteszYh25Ue0G-LSauv3_h5v8Hg5hKY0M6H3_J-71AQ5JG4Ol9dL_qqo9Tgq1hvMyBVWRqO8SVasRJL2MCMNY-1SgW7uZ5uMetLZ7x6BKazH868aqASWFa7vz1ah6iO2sE4abC1mqP7rwDtczVG-yVsdkA398g7s69DGwrUQIXPKjrOeEmLgaXZOVG3-Q3wmR6u1phoB0XtI-HK1dSIg7BLFaTYJMR7tjAt9H6yIInN0d02tD3egP_oReuOXohKT5UZnN223OnuHo27Y9qIITv4IVU9yYeZJjNLqnRXgLZY7MZzDGJFBVNYu9k9FtiYbe0UpjcemM89KIXiZVO712cNSnfxDwW4lsn_pQx42OuA7e07P01cePyG-RlOg6QN57foBuVkA2qzKk6-PfZmcrDn64F_5d37rYGMtfzWzzB3mBSkN7zaCFUxG5LrXz9ZZK8wBI5pF27Fjm1WuJTgnxOrrkL1rmFiqv1_gPTw7JGydsEa-Gm5ZP5rEDJ521608bm_aRyjWJbQJUb-vgapK29lUJ-gKj4rd5xdSePGIn3k.ldtFkUXS7v316YzUGUjx1w"
            )
            .url(url)
            .build()
        messageListener = _messageListener
    }

    fun initClient(url: String) {
        val client: OkHttpClient = OkHttpClient.Builder()
            .writeTimeout(5, TimeUnit.SECONDS)
            .readTimeout(5, TimeUnit.SECONDS)
            .connectTimeout(10, TimeUnit.SECONDS)
            .build()
        val request: Request = Request.Builder()
            .header("X-User-ID", "ronaldo")
            .header(
                "Authorization",
                "Bearer eyJhbGciOiJkaXIiLCJjdHkiOiJKV1QiLCJlbmMiOiJBMjU2R0NNIiwidHlwIjoiSldUIiwiemlwIjoiREVGIn0..ns0kcYFjPpBF9cZd.et_F2zVZc8Kf7QH9a5w6xypFOTfCrX10DbzwWJu57gucE2yoi4k-DqTsTQtmlw_HTAs60nMWwFPjdL7NLxNCPPqAg7rGNzVm79wfOgp05-xFvH5Iv6jt5Yo0949msDF0j0RQ4iHn6G9wa_TfFiWXcmMwUfviN1pWI3K2K6qmKaQY-fefo4mJ0bZ_UNQGzHUU_RcM5bQIZpZ-RmB0-iM3ToE-KF1SWEEDGBD948KgfDjHfbxPVyixl5hSXYK9QSA82kW8e887Hybdix1enDPiLVhT-3gj4N0e7SalgjpT4AHfKoL1KJhH5Zfr9_9kRdAWy0VmE_cXLDkldjlJR1C8djyeyKcwzgk-V6Nb_-jLsN2zZzf2BUDn1IPmySeDYsIcYMOuhV6tqZHJpXKMQpOp9uteszYh25Ue0G-LSauv3_h5v8Hg5hKY0M6H3_J-71AQ5JG4Ol9dL_qqo9Tgq1hvMyBVWRqO8SVasRJL2MCMNY-1SgW7uZ5uMetLZ7x6BKazH868aqASWFa7vz1ah6iO2sE4abC1mqP7rwDtczVG-yVsdkA398g7s69DGwrUQIXPKjrOeEmLgaXZOVG3-Q3wmR6u1phoB0XtI-HK1dSIg7BLFaTYJMR7tjAt9H6yIInN0d02tD3egP_oReuOXohKT5UZnN223OnuHo27Y9qIITv4IVU9yYeZJjNLqnRXgLZY7MZzDGJFBVNYu9k9FtiYbe0UpjcemM89KIXiZVO712cNSnfxDwW4lsn_pQx42OuA7e07P01cePyG-RlOg6QN57foBuVkA2qzKk6-PfZmcrDn64F_5d37rYGMtfzWzzB3mBSkN7zaCFUxG5LrXz9ZZK8wBI5pF27Fjm1WuJTgnxOrrkL1rmFiqv1_gPTw7JGydsEa-Gm5ZP5rEDJ521608bm_aRyjWJbQJUb-vgapK29lUJ-gKj4rd5xdSePGIn3k.ldtFkUXS7v316YzUGUjx1w"
            )
            .url(url)
            .build()

        client.newWebSocket(request, createListener())
    }

    /**
     * connect
     */
    fun connect() {
        if (isConnect()) {
            Log.i(TAG, "web socket connected")
            return
        }
        client.newWebSocket(request, createListener())
    }

    /**
     * Reconnection
     */
    fun reconnect() {
        if (connectNum <= MAX_NUM) {
            try {
                Thread.sleep(MILLIS.toLong())
                connect()
                connectNum++
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
        } else {
            Log.i(
                TAG,
                "reconnect over $MAX_NUM,please check url or network"
            )
        }
    }

    /**
     * Whether to connect
     */
    fun isConnect(): Boolean {
        return isConnect
    }

    /**
     * send messages
     *
     * @param text string
     * @return boolean
     */
    fun sendMessage(text: String): Boolean {
        return if (!isConnect()) false else mWebSocket.send(text)
    }

    /**
     * send messages
     *
     * @param byteString character set
     * @return boolean
     */
    fun sendMessageByte(byteString: ByteString): Boolean {
        return if (!isConnect()) false else mWebSocket.send(byteString)
    }

    /**
     * Close connection
     */
    fun close() {
        if (isConnect()) {
            mWebSocket.cancel()
            mWebSocket.close(1001, "The client actively closes the connection ")
        }
    }

    private fun createListener(): WebSocketListener {
        return object : WebSocketListener() {
            override fun onOpen(
                webSocket: WebSocket,
                response: Response
            ) {
                super.onOpen(webSocket, response)
                Log.d(TAG, "open:$response")
                mWebSocket = webSocket
                isConnect = response.code == 101
                if (!isConnect) {
                    reconnect()
                } else {
                    Log.i(TAG, "connect success.")
                    messageListener.onConnectSuccess()
                }
            }

            override fun onMessage(webSocket: WebSocket, text: String) {
                super.onMessage(webSocket, text)
                messageListener.onMessage(text)
            }

            override fun onMessage(webSocket: WebSocket, bytes: ByteString) {
                super.onMessage(webSocket, bytes)
                val msg = generateMessageIndo(bytes.toByteArray())
                Log.i(TAG, "connect onMessage bytes.${msg}")
                messageListener.onMessage(msg.toString())
            }

            override fun onClosing(
                webSocket: WebSocket,
                code: Int,
                reason: String
            ) {
                super.onClosing(webSocket, code, reason)
                isConnect = false
                messageListener.onClose()
            }

            override fun onClosed(
                webSocket: WebSocket,
                code: Int,
                reason: String
            ) {
                super.onClosed(webSocket, code, reason)
                isConnect = false
                messageListener.onClose()
            }

            override fun onFailure(
                webSocket: WebSocket,
                t: Throwable,
                response: Response?
            ) {
                super.onFailure(webSocket, t, response)
                if (response != null) {
                    Log.i(
                        TAG,
                        "connect failed：" + response.message
                    )
                }
                Log.i(
                    TAG,
                    "connect failed throwable：" + t.message
                )
                isConnect = false
                messageListener.onConnectFailed()
                reconnect()
            }
        }
    }
}

class BasicAuthInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        TODO("Not yet implemented")
    }

}