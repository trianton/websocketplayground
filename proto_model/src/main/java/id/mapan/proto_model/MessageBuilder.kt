package id.mapan.proto_model

/**
 * Created by Naupal T. on 02/11/21.
 */


fun generateIndoMssage():MessageProto.IndonesianMessage{
    return MessageProto.IndonesianMessage.newBuilder().setPengirim("dari Mobile Nih").setRoomId("e88bf595-7472-41d0-82ff-17b2213a412c").setTeks("ini message coba2 lagi yaa").build()
}

fun generateMessageIndo(): ByteArray? {
    return MessageProto.Message.newBuilder()
        .setIndonesianMessage(generateIndoMssage())
        .build().toByteArray()
}

fun generateMessageIndo(bytes:ByteArray): MessageProto.Message {
    return MessageProto.Message.parseFrom(bytes)

}